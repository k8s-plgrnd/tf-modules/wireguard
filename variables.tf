variable "cidr" {
  type    = string
  default = "172.16.0.0/16"
}

variable "servers" {
  type = list(object({
    id         = string
    public_ip  = string
    private_ip = string
  }))
  default = []
}
