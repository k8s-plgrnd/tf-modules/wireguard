provider "null" {
  version = "~> 2.1"
}
provider "external" {
  version = "~> 1.2"
}

locals {
  interface = "wg0"
  port      = 51820
  keyfile   = "/etc/wireguard/key"
  conf      = "/etc/wireguard/${local.interface}.conf"
  servers = [
    for i, server in var.servers : merge(server, { "vpn_ip" : cidrhost(var.cidr, i + 1) })
  ]
}

resource "null_resource" "generate_keys" {
  count = length(local.servers)

  connection {
    host  = local.servers[count.index].public_ip
    user  = "root"
    agent = true
  }

  provisioner "remote-exec" {
    inline = [
      "test -f '${local.keyfile}' || wg genkey > '${local.keyfile}'",
      "wg pubkey < '${local.keyfile}' > '${local.keyfile}.pub'",
      "chmod 0600 '${local.keyfile}'",
    ]
  }
}

data "external" "pubkeys" {
  depends_on = [null_resource.generate_keys]
  count      = length(local.servers)

  program = [
    "sh",
    "-c",
    "jq -n --arg pubkey \"$(ssh -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' root@${local.servers[count.index].public_ip} cat ${local.keyfile}.pub)\" '{$pubkey}'",
  ]
}

resource "null_resource" "wireguard-configure" {
  count = length(local.servers)

  # Regenerate configuration when servers change
  triggers = {
    server_ids = "${join(",", local.servers.*.id)}"
  }

  connection {
    host  = local.servers[count.index].public_ip
    user  = "root"
    agent = true
  }

  # Set up WireGuard configuration files
  provisioner "file" {
    destination = local.conf
    content     = <<EOC
[Interface]
  Address=${cidrhost(var.cidr, count.index + 1)}/16
  ListenPort=${local.port}
  PrivateKey=

%{for i, server in local.servers}
%{~if i != count.index~}
[Peer]
  PublicKey=${data.external.pubkeys[index(local.servers, server)].result.pubkey}
  AllowedIPs=${server.vpn_ip}/32
  Endpoint=${server.private_ip}:${local.port}

%{~endif~}
%{endfor}
EOC
  }

  # Configure UFW
  provisioner "remote-exec" {
    inline = [<<EOS
%{for i, server in local.servers}
  %{if i != count.index}
    ufw allow \
      from ${server.private_ip} \
      to ${local.servers[count.index].private_ip} \
      port ${local.port} \
      proto udp \
      comment 'WireGuard peer'
  %{endif}
%{endfor}
EOS
    ]
  }

  # Configure WireGuard
  provisioner "remote-exec" {
    inline = [
      "sed -i \"s|PrivateKey=|PrivateKey=$(cat ${local.keyfile})|\" '${local.conf}'",
      "chmod 0600 '/etc/wireguard/${local.interface}.conf'",
      "wg-quick down wg0; wg-quick up wg0",
      "systemctl enable wg-quick@wg0.service",
    ]
  }
}
