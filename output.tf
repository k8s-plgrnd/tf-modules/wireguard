output servers {
  value = local.servers
}
output interface {
  value = local.interface
}
